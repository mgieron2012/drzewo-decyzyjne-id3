from math import inf, log
from random import random
import argparse


def groupBy(data, attr):
    groups = {}

    for row in data:
        val = row[attr]
        if val in groups:
            groups[val].append(row)
        else:
            groups[val] = [row]
    return groups


def getMostFrequentValue(data, attr):
    groups = groupBy(data, attr)
    return sorted(map(lambda key: (len(groups[key]), key), groups))[-1][1]


class TreeID3:
    def __init__(self, data, default_value=0):
        self.isLeaf = True
        self.value = 0
        self.predictionAttribute = 0
        self.build(data, default_value)
        self.default_value = default_value

    def predict(self, data):
        if self.isLeaf:
            return self.value
        else:
            if data[self.predictionAttribute] in self.value:
                return self.value[data[self.predictionAttribute]].predict(data[:self.predictionAttribute] + data[self.predictionAttribute + 1:])
            else:
                return self.default_value

    def build(self, data, default_value):
        if all(map(lambda x: data[0][0] == x[0], data)):
            self.value = data[0][0]
        elif len(data[0]) == 1:
            self.value = getMostFrequentValue(data, 0)
        else:
            default_value = getMostFrequentValue(data, 0)
            best_attr = 0
            min_ent = inf
            self.value = {}

            for i in range(1, len(data[0])):
                if (ent := self.calcInf(data, i)) < min_ent:
                    min_ent = ent
                    best_attr = i

            groups = groupBy(data, best_attr)

            for key, group in groups.items():
                self.value[key] = TreeID3(
                    list(map(lambda x: x[:best_attr] + x[best_attr + 1:], group)), default_value)
            self.isLeaf = False
            self.predictionAttribute = best_attr

    @staticmethod
    def calcInf(data, attr):
        rows_number = len(data)
        result = 0

        groups = groupBy(data, attr)

        for group in groups:
            res_groups = groupBy(group, -1)
            IU = 0

            for res_group in res_groups:
                freq = len(res_group) / len(group)
                IU -= freq * log(freq)

            result += len(group) / rows_number * IU

        return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-source_file_name', default="breast-cancer.data")
    source_file_name = parser.parse_args().source_file_name
    
    data = []

    with open(source_file_name, "r") as f:
        data = f.readlines()
        data = list(map(lambda x: x.rstrip().split(","), data))

    build_data = []
    test_data = []

    for row in data:
        if random() > 0.4:
            build_data.append(row)
        else:
            test_data.append(row)

    tree = TreeID3(build_data)

    results = {}

    for row in test_data:
        if row[0] not in results:
            results[row[0]] = [0, 0]

        results[row[0]][0 if tree.predict(row) == row[0] else 1] += 1

    
    print(results)
    
    print(sum(map(lambda x: x[0], results.values())) / len(test_data))
