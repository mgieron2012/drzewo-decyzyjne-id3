# Drzewo decyzyjne ID3

## Zadanie

Zaimplementować klasyfikator ID3 (drzewo decyzyjne). Atrybuty nominalne, testy tożsamościowe. Podać dokładność i macierz pomyłek na zbiorach: Breast cancer i mushroom. Sformułować i spisać w kilku zdaniach wnioski.

## Wyniki

Po 3 testy na dwóch zbiorach danych

### GRZYBY

Próba 1

<table>
    <tr>
        <td colspan="2" rowspan="2"></td>
        <th colspan="2">Wartość prawdziwa</th>
    </tr>
    <tr>
        <th>Jadalne</th>
        <th>Niejadalne</th>
    </tr>
    <tr>
        <th rowspan="2">Wartość przewidywana</th>
        <th>Jadalne</th>
        <td>1674</td>
        <td>3</td>
    </tr>
    <tr>
        <th>Niejadalne</th>
        <td>4</td>
        <td>1561</td>
    </tr>
    <tr>
        <th colspan="2">Ogólna dokładność</th>
        <td colspan="2">0.996295152824946</td>
    </tr>
</table>

Próba 2

<table>
    <tr>
        <td colspan="2" rowspan="2"></td>
        <th colspan="2">Wartość prawdziwa</th>
    </tr>
    <tr>
        <th>Jadalne</th>
        <th>Niejadalne</th>
    </tr>
    <tr>
        <th rowspan="2">Wartość przewidywana</th>
        <th>Jadalne</th>
        <td>1693</td>
        <td>8</td>
    </tr>
    <tr>
        <th>Niejadalne</th>
        <td>2</td>
        <td>1541</td>
    </tr>
    <tr>
        <th colspan="2">Ogólna dokładność</th>
        <td colspan="2">0.9969173859432799</td>
    </tr>
</table>

Próba 3

<table>
    <tr>
        <td colspan="2" rowspan="2"></td>
        <th colspan="2">Wartość prawdziwa</th>
    </tr>
    <tr>
        <th>Jadalne</th>
        <th>Niejadalne</th>
    </tr>
    <tr>
        <th rowspan="2">Wartość przewidywana</th>
        <th>Jadalne</th>
        <td>1665</td>
        <td>5</td>
    </tr>
    <tr>
        <th>Niejadalne</th>
        <td>0</td>
        <td>1588</td>
    </tr>
    <tr>
        <th colspan="2">Ogólna dokładność</th>
        <td colspan="2">0.9969173859432799</td>
    </tr>
</table>


### NOWOTWÓR

Próba 1

<table>
    <tr>
        <td colspan="2" rowspan="2"></td>
        <th colspan="2">Wartość prawdziwa</th>
    </tr>
    <tr>
        <th>Tak</th>
        <th>Nie</th>
    </tr>
    <tr>
        <th rowspan="2">Wartość przewidywana</th>
        <th>Tak</th>
        <td>14</td>
        <td>18</td>
    </tr>
    <tr>
        <th>Nie</th>
        <td>17</td>
        <td>67</td>
    </tr>
    <tr>
        <th colspan="2">Ogólna dokładność</th>
        <td colspan="2">0.6982758620689655</td>
    </tr>
</table>

Próba 2

<table>
    <tr>
        <td colspan="2" rowspan="2"></td>
        <th colspan="2">Wartość prawdziwa</th>
    </tr>
    <tr>
        <th>Tak</th>
        <th>Nie</th>
    </tr>
    <tr>
        <th rowspan="2">Wartość przewidywana</th>
        <th>Tak</th>
        <td>16</td>
        <td>11</td>
    </tr>
    <tr>
        <th>Nie</th>
        <td>18</td>
        <td>66</td>
    </tr>
    <tr>
        <th colspan="2">Ogólna dokładność</th>
        <td colspan="2">0.7387387387387387</td>
    </tr>
</table>

Próba 3

<table>
    <tr>
        <td colspan="2" rowspan="2"></td>
        <th colspan="2">Wartość prawdziwa</th>
    </tr>
    <tr>
        <th>Tak</th>
        <th>Nie</th>
    </tr>
    <tr>
        <th rowspan="2">Wartość przewidywana</th>
        <th>Tak</th>
        <td>15</td>
        <td>10</td>
    </tr>
    <tr>
        <th>Nie</th>
        <td>18</td>
        <td>65</td>
    </tr>
    <tr>
        <th colspan="2">Ogólna dokładność</th>
        <td colspan="2">0.7407407407407407</td>
    </tr>
</table>

## Wnioski

- prosta implementacja algorytmu
- szybkie wykonanie budowy drzewa i przewidywania
- algorytm jest uniwersalny dla danych z różnych dziedzin
- przy wystarczająco dużych danych algorytm jest w stanie przewidywać wyniki z blisko 100% skutecznością - pytanie tylko czy 
satysfakcjonuje nas to, że algorytm zakwalifikuje niejadalnego grzyba jako jadalnego tylko raz na 800 testów
- gdy w danych wykorzystanych do budowania drzewa przeważa jedna z klas, algorytm będzie przewidywał ją dużo częściej, co 
spowoduje, że skuteczność wykrywania rzadziej występującej klasy będzie niska (w testach poniżej 50%)
- jest możliwe faworyzowanie bezpieczniejszych predykcji, np. klasyfikowania grzybów jako niejadalnych, 
z prawdopodobnym spadkiem ogólnej skuteczności
